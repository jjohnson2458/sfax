There are several classes within the TSfax class that came from Sfax and are used for their specialized encryption. They are located in development at:

.../shared/shared_thirdparty/elgon/sfax/

Currently, we are only concerned with (and will use) 3 of the classes in the folder:

TSfax.php (parent)
TSfaxIncomingCron.php
TSfaxOutgoingCron.php

TSfax is the parent class to the corresponding child classes. Their names explain their functions. Among them, two database tables are used currently: client_faxin for incoming faxes and client_faxqueue for outgoing faxes. Among the limitations of Sfax is the requirement that a fax (pdf file) must be present from the source of the api request. For this reason and testing, two sub-folders were created and are used for incoming and outgoing faxes called incoming and outgoing, respectively. These two folders must have full write permissions granted. Upon moving to a production environment, the exact locations of these two folders can be altered, but it should be noted that file paths are stored in the database fax tables are relative the the path set in the class at the time the record is written. (note: although the code is currently in the development area, the incoming and outgoing folders currently reside in /home/gps/live/_tempdir/. See Outstanding Issues)

THE SFAX API

Sfax has a code library for their API, but the scripts were all repetitive. In this TSfax class, the api�s available via the dynamic magic method TSfax::__call(). Any request to Sfax (uploading, downloading, checking statuses) are ran through this method. Due to the configurations on the Secure Web Server, extra curl parameters had to be added. If during code review and method is called that does not have the �fn� prefix, in most cases it will be a dynamic call to Sfax instead of a hard-coded method. This was done to streamline all API requests.

THE MESSAGE QUEUE

Throughout each TSfax session, key events and/or errors are logged into a message queue array (TSfax::$aMessages). After a completed process, these messages can be accessed via (TSfax::fnGetMessages()), or upon fax upload, the referenced array $cOptions.

SENDING FAXES

Contained the parent class is a key method 

TSfax::fnSendFax($sFilePath, $sFaxNumber , $cOptions)

This is the a legacy method that sends a fax via Sfax without using the Fax Queue. To add a fax to the fax queue for sending, use:

if path to media is known

TSfax::fnAddToFileIdToQueue($sFilePath, $sFaxNumber, $cOptions) 

If fileId is obtained from the gps_files table. In all cases, the class validates the phone number and transposes it to a 1 + 10 digits.

The class TSfaxOutgoingCron will handle sending (uploading) faxes to the Sfax queue, but as per Sfax�s suggestion, only a maximum of 99 files can be in the Sfax queue at a time per account (see Account Limitations). In each session,  TSfaxOutgoingCron::fnCronSendFaxes() loads the maximum allowed faxes from the queue that have not been �uploaded� to Sfax, and does so using their API.  TSfaxOutgoingCron::fnCronCheckFaxes() can be called next to use the API to check the status of any faxes sent to the Sfax queue, since a few minutes may have passed since completion of the fax send. 

Among the last safeguards added was a flag for each fax to be set during its upload in the event there is a backlog in the batches of faxes uploaded. The class is designed not to attempt delivering the same fax more than once per record.

RECEIVING FAXES

As requested, TSfaxIncomingCron handles the incoming faxes by making a request to Sfax to receive the status of all faxes within a given time frame. Each incoming FaxId is checked against FaxId�s that are not marked as downloaded, and attempts the download. At the time of this writing, safeguards are still needed to avoid a backlog if/when large numbers of faxes are incoming.

DATA TABLES

The schemas for each table used by Sfax is stored in TSfax as fnInitSfaxInTable ans fnInitSfaxOutTable respectively. Errors when both receiving and sending faxes are logged in the corresponding database table. Also, the raw record for each incoming and/or outgoing fax is serialized and written into the column �FAXDATA� in both respective tables. Any barcode information from incoming faxes will also be saved, but the ability of Sfax to electronically read a barcode from an incoming fax has not been testing as of the time of this writing.

THE AUTO CRON

[deleted for proprietary purposes]