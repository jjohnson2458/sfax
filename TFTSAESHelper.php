<?php

class TFTSAESHelper
{
    protected $pTokenContext; 
    protected $pTokenUsername;
    protected $pTokenApiKey;
    protected $pTokenClient; 
    protected $pEncryptionKey; 
    protected $pEncryptionInitVector;

	/**
	*
	*
	*/
    public function __construct($pSecurityContext){
    $this->pTokenContext=$pSecurityContext;                        
        $this->pTokenUsername="JJJohnson";  //<--- IMPORTANT: Enter a valid Username
        $this->pTokenApiKey=  "350FE8A0B33547228E8BA01FB3AE8136";  //<--- IMPORTANT: Enter a valid ApiKey
        $this->pTokenClient="";   //<--- IMPORTANT: Leave Blank
        $this->pEncryptionKey="L_H3fPN7rP8ub_#gKrnN3ECJkG_bymEN";  //<--- IMPORTANT: Enter a valid Encryption key
        $this->pEncryptionInitVector="x49e*wJVXr8BrALE";  //<--- IMPORTANT: Enter a valid Init vector
    }
	/**
	*
	*
	*/
    public function GenerateSecurityTokenUrl(){
        $tokenDataInput;
        $tokenDataEncoded;
        $tokenGenDT;
        $tokenGenDT = gmdate("Y-m-d") . "T" . gmdate("H:i:s") . "Z";
        $tokenDataInput = "Context=" . $this->pTokenContext . "&Username=" . $this->pTokenUsername. "&ApiKey=" . $this->pTokenApiKey . "&GenDT=" . $tokenGenDT . "";
        if($this->pTokenClient != null && $this->pTokenClient != "")
        {
            $tokenDataInput .= "&Client=" . $this->pTokenClient;
        }
        $AES = new TAES_Encryption($this->pEncryptionKey, $this->pEncryptionInitVector, "PKCS7", "cbc");
        $tokenDataEncoded = base64_encode($AES->encrypt($tokenDataInput));
        return $tokenDataEncoded;
    }	
}