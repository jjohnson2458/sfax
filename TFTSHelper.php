<?php

class TFTSHelper
{
    public static function GetHeaders($responseBody, $responseInfo){
        $header_text = substr($responseBody, 0, $responseInfo['header_size']);
        $headers = array();
        foreach(explode("\n",$header_text) as $line){
            $parts = explode(": ",$line);
            if(count($parts) == 2){
                if (isset($headers[$parts[0]])){
                    if (is_array($headers[$parts[0]])) $headers[$parts[0]][] = chop($parts[1]);
                    else $headers[$parts[0]] = array($headers[$parts[0]], chop($parts[1]));
                } else {
                    $headers[$parts[0]] = chop($parts[1]);
                }
            }
        }
        return $headers;        
    }	


	/**
	*
	* @param
	* @param
	* @return
	*/
	public static function GetResponseData($responseBody, $responseInfo){
        $body = "" . substr($responseBody, $responseInfo['header_size']);
        echo "SendFaxResponse: " . $body;		
	}

	/**
	*
	* @param
	* @param
	* @return
	*/
	public static function GetInboundResponseData($responseBody, $responseInfo){
        $body = "" . substr($responseBody, $responseInfo['header_size']);
        echo "InboundRetrieveSets: " . $body;		
	}


	/**
	*
	* @param
	* @param
	* @return
	*/
	public static function WriteResponseToFile($responseBody, $responseInfo, $localFileName){
        $data = substr($responseBody, $responseInfo['header_size']);
        $fp = fopen($localFileName, "w");
        fwrite($fp, $data, strlen($data));
        fclose($fp);		
	}



}

