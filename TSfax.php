<?php
/**
* Sends and Receives faxes using SFax
* 
*
*/

/**
* This class is the fax handler for gps via Sfax
* 
* @author J.Johnson <jj@elgonsystems.com>*
* @package sfax
* @see http://sfax.scrypt.com/article/326-code-samples-php
* @todo fnGetFax
* @todo fnGetFaxList

*
* 
*/



class TSfax 
{


	/**
	* @var string	sfax primary api key
	*/
	
	CONST SFAX_API_KEY = '<deleted>';


	/**
	* @var int	sfax primary fax number
	*/
	//CONST SFAX_NUMBER = '8448182427';
	//CONST SFAX_NUMBER = '5086894644';
	CONST SFAX_NUMBER = '8449659573';

	/**
	* @var string - REST API url for sfax
	* @access protected 
	*/
    protected $serviceEndpointUrl;


	/**
	* @var string - input security context, if any
	* @access protected 
	*/
    protected $securityContext;

	/**
	* @var string - valid token supplied by sfax
	* @access protected 
	*/
    protected $securityToken;

	/**
	* @var string - valid API key supplied by sfax
	* @access protected 
	*/
    protected $sApiKey;

	/**
	* @var string - used to define range for incoming faxes
	* @access protected 
	*/
	protected $sStartDate;

	/**
	* @var string - used to define range for incoming faxes
	* @access protected 
	*/
	protected $sEndDate;

	/**
	* @var int - used to set limits on the amount of incoming faxes
	* @access protected 
	*/
	protected $iMaxItems = 500;

	/**
	* @var int - used to set limits on the amount of outgoing faxes
	* @access protected 
	*/
	protected $iFaxQueueLimit = 99;



	/**
	* @var array - to define allowed faxtypes
	* @access protected 
	*/
	protected $faxtypes = array('pdf', 'tif');

	/**
	* @var object - internal CURL object for RESTful interface with Sfax
	* @access protected 
	*/
	protected $ch = null;

	/**
	* @var array  - messages generated during session
	* @access protected 
	*/
	protected $aMessages = array();

	/**
	* @var string - default recipient name
	* @access protected 
	*/
	protected $defaultName = 'default';

	/**
	* @var string - alpha-numeric id return upon successful fax transmission
	* @access protected 
	*/
	protected $faxid;

	/**
	* @var string - inherited from TFTSHelper
	* @access protected 
	*/
    protected $pTokenContext; 

	/**
	* @var string - inherited from TFTSHelper
	* @access protected 
	*/
    protected $pTokenUsername;



	/**
	* @var string - inherited from TFTSHelper
	* @access protected 
	*/
    protected $pTokenClient; 

	/**
	* @var string - inherited from TFTSHelper
	* @access protected 
	*/
    protected $pEncryptionKey; 

	/**
	* @var string - inherited from TFTSHelper
	* @access protected 
	*/
    protected $pEncryptionInitVector;

	/**
	* @var array - inherited from TFTSHelper
	* @access protected 
	*/
	protected $options = array();

	/**
	* class constructor
	* @return void
	*/

	/**
	* @var int - x position of barcode
	* @access protected 
	*/
	protected $BarcodeX = 1500;

	/**
	* @var int - y position of barcode
	* @access protected 
	*/
	protected $BarcodeY = 2100;


	/**
	* @var int - default size of barcode
	* @access protected 
	*/
	protected $BarcodeScale = 5;


	/**
	* @var int - default size of barcode
	* @access protected 
	*/
	protected $BarcodePage = 1;


	/**
	* @var string - path to temp directory; derived from gps constant
	* @access pubic 
	*/
	public $tempDir;  


	/**
	* @var string - path to temp location for incoming faxes (test mode only)
	* @access pubic 
	*/
	public $tempBaseName = 'incomingFax';  

	/**
	* @var string - designated store path for faxes
	* @access protected 
	*/
	protected $storepath = 'gpsfax';


	/**
	* @var string - default date format
	* @access protected 
	*/
	protected $sDateFormat = 'Y-m-d \TH:i:s\Z';


	/**
	* @var int - gps client variable
	* @access public
	* @todo - make protected variable
	*/
	public $clientId;

	/**
	* @var int - gps user variable
	* @access protected 
	*/
	protected $userId;

	/**
	* class constructor
	*
	*/
    public function __construct (){

		$this->iTimeLimit = ini_get('time_limit');
		$this->sMemoryLimit = ini_get('memory_limit');

		set_time_limit(0);
		ini_set('memory_limit', '1024M');

		$this->db = new mysqli(_DBSERVER_,_DBUSER_, _DBPWD_,_DBNAME_);
		if (!$this->db || $this->db->connect_error) {
			return $this->fnSetFaxError($this->db->connect_error);
		}


		if (function_exists('FN_CLIENTID')) {
			$this->clientId =  FN_CLIENTID();
		}

		if (function_exists('FN_USERID')) {
			$this->userId =  FN_USERID();
		}

		$this->sFaxInTable = 'client_faxin';
		$this->sFaxOutTable = 'client_faxqueue';

		$this->fnInit();
		// set root inbound/outbound paths for faxes ===========================================================================

		if (defined('tempDir')) {
			$this->tempDir					= realpath(tempDir);
			$this->sInboundFaxPath			= $this->tempDir . '/incoming/';
			$this->sOutboundFaxPath			= $this->tempDir . '/outgoing/';			
		} else {
			$this->sRootPath				= (php_sapi_name() == 'cli') ? $_SERVER['PWD'] : $_SERVER['DOCUMENT_ROOT'] ;
			$this->sRootPath				= rtrim($this->sRootPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
			$this->sRootPath				= __DIR__;

			$this->sInboundFaxPath			= $this->sRootPath . DIRECTORY_SEPARATOR . 'incoming' . DIRECTORY_SEPARATOR;
			$this->sOutboundFaxPath			= $this->sRootPath . DIRECTORY_SEPARATOR . 'outgoing' . DIRECTORY_SEPARATOR;			
		}
		
		// ====================================================================================================================

		$this->fnInitDB();
		$bReady = $this->fnSfaxInit(); // validates writable folders, and database connection.
		if (!$bReady) {
			echo '<pre>'; print_r($this->aMessages);
			exit();
		}


        $this->serviceEndpointUrl		= "https://api.sfaxme.com/api/";
        $this->securityContext			= ""; //<--- Required but leave blank exactly as it is here
        $this->sApiKey					= self::SFAX_API_KEY; //Required Key 
		$this->aBarcodeElements			= array('BarcodeData' => '', 'BarcodeX' => '1500', 'BarcodeY' => '2100','BarcodeScale' => '5', 'BarcodePage' => '1');

		$this->pTokenContext			= ""; //$pSecurityContext;                        
        $this->pTokenUsername			= "century.jj";  //<--- IMPORTANT: Enter a valid Username

        $this->sApiKey					=  "<deleted>";  //<--- IMPORTANT: Enter a valid ApiKey

        $this->pTokenClient				= "";   //<--- IMPORTANT: Leave Blank
        $this->pEncryptionKey			= '<deleted>';  //<--- IMPORTANT: Enter a valid Encryption key

        $this->pEncryptionInitVector	= "<deleted vector>";  //<--- IMPORTANT: Enter a valid Init vector	



		$this->fnSetEndDate();
		$this->fnSetStartDate(date('m/d/Y', (strtotime($this->sEndDate) - 86400))); // default start date set to 24 hours prior to end date
		

		
	}

	/**
	* returns true only if all conditions met for Sfax API
	* @param
	* @param
	* @return bool
	*/
	public function fnSfaxInit(){
		$bInboundUsable = false;
		if (!is_dir($this->sInboundFaxPath)) {
			$this->fnSetFaxError($this->sInboundFaxPath . ' does not exists.');
			if (!@mkdir($this->sInboundFaxPath, 0777, true)) {
				$this->fnSetFaxError("Could not create the directory: " . $this->sInboundFaxPath);
			}
		} elseif (!is_writable($this->sInboundFaxPath)) {
			$this->fnSetFaxError("The directory " . $this->sInboundFaxPath . " is not writable" );
		} else {
			$bInboundUsable = true;
		}

		$bOutboundUsable = false;
		if (!is_dir($this->sOutboundFaxPath)) {
			$this->fnSetFaxError($this->sOutboundFaxPath . ' does not exists.');
			if (!@mkdir($this->sOutboundFaxPath, 0777, true)) {
				$this->fnSetFaxError("Could not create the directory: " . $this->sOutboundFaxPath);
			}
		} elseif (!is_writable($this->sOutboundFaxPath)) {
			$this->fnSetFaxError("The directory " . $this->sOutboundFaxPath . " is not writable" );
		} else {
			$bOutboundUsable = true;
		}	
	
		return 	$bInboundUsable && $bOutboundUsable && $this->fnHasConnection(true);

	}
	



	/**
	* independent db initialization since this is running on a cron
	* and may be handling hundress of sessions as once
	* @return bool
	*/
	public function fnInitDB(){
		$this->db = mysqli_init();
		if (!$this->db) {
			die('mysqli_init failed');
		}
		if (!$this->db->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5)) {
			die('Setting MYSQLI_OPT_CONNECT_TIMEOUT failed');
		}
		if (!$this->db->real_connect(_DBSERVER_,_DBUSER_, _DBPWD_,_DBNAME_)) {
			return $this->fnSetFaxError('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		} else {
			return true;
		}

	}
	

	/**
	* returns validation of a database connection
	* @param bool $bMessage - on true will add connection error to message queue
	* @return bool
	*/
	public function fnHasConnection($bMessage = false){
		if ($bMessage) {
			return (!$this->db->connect_error) ? true : $this->fnSetFaxError("No database connection.");	
		}
		return (!$this->db->connect_error) ? true : false;		
	}


	/**
	* for self-containment of all other related files
	* not require for live mode;
	* @return void
	*/
	function fnInit(){
		if (!defined('tempDir')) {
			define('tempDir', __DIR__);
		}

		if (!class_exists('TpadCrypt')) {
			require_once(__DIR__ . '/TpadCrypt.php');
		}

		if (!class_exists('TAES_Encryption')) {
			require_once(__DIR__ . '/TAES_Encryption.php');
		}
	
					
	}

	


	/**
	* adds full path of pdf to gps fax queue
	* @param string $sFilePath - valid file path
	* @param string $sSendTo fax recipient
	* @param string $sSentFrom  sender of fax
	* @param array $cOptions - reference array for input and output of data 
	* @return bool - true if data is saved
	* @see TSfax::fnInitFaxTables();
	*/
	public function fnAddToFaxQueue($sFilePath = false, $sSendTo = "", $sSentFrom = "", &$cOptions = array()){
		$sFilePath = $this->fnValidateFaxPath($sFilePath);
		$sFaxToNumber = $this->fnValidateFaxNumber($sSendTo);
		if ($sFaxToNumber) {
			$aParams = array();
			$sFaxFromNumber = ($this->fnValidateFaxNumber($sSentFrom)) ? $this->fnValidateFaxNumber($sSentFrom): $this->fnValidateFaxNumber(self::SFAX_NUMBER);
			if (isset($cOptions['barcodes'])) {
				$aParams[] = "`BARCODES` = '" . $this->fnEscape(serialize($cOptions['barcodes'])) . "'";
			}
			if (isset($cOptions['pages'])) {
				$aParams[] = "`FAXPAGES` = '" . int($cOptions['pages']) . "'";
			}
			if (isset($cOptions['fileid'])) {
				$aParams[] = "`FILEID` = '" . int($cOptions['fileid']) . "'";				
				$iFileSize = '1';
			} elseif($sFilePath) {
				$iFileSize = filesize($sFilePath);				
			}
			$aParams[] = "`FILESIZE` = '" . $iFileSize . "'";
			$aParams[] = "`CLIENTID` = '" . $this->clientId . "'";
			$aParams[] = "`TOFAXNUMBER` = '" . $sFaxToNumber . "'";
			$aParams[] = "`FROMFAXNUMBER` = '" . $sFaxFromNumber . "'";
			$aParams[] = "`DATECREATED` = '" . $this->fnGetLocalTime() . "'";
			$aParams[] = "`FAXDATETIME` = '" . $this->fnGetLocalTime()  . "'";
			$aParams[] = "`FILEPATH` = '" . $this->fnEscape(str_replace($this->sOutboundFaxPath, '', $sFilePath)) . "'";

			if ($iFileSize > 0) {
				$qSQL = "INSERT INTO `{$this->sFaxOutTable}` SET " . join(', ', $aParams);
				$this->db->query($qSQL);
				if ($this->db->errno) {
					$this->fnSetFaxError($this->db->error);
				} else {
					$this->fnSetFaxSuccess($sFilePath . ' has been added to fax queue: ' . $this->sFaxOutTable);
					return $this->db->insert_id;
				}				
			} else {
				return $this->fnSetFaxError("Invalid Filesize. Did not save to Fax Queue.");
			}



		}

	}

	
	/**
	* adds to fax queue using the gps file id
	* @param int $iFileId - file identifier from gps_files
	* @param string $sSendTo - fax recipient
	* @param string $sSentFrom  sender of fax
	* @param array $cOptions - reference array for input and output of data 
	* @return string $sSentFrom - sender of fax
	* @see TSfax::fnAddToFaxQueue()
	*/
	public function fnAddFileIdToFaxQueue($iFileId = "", $sSendTo = "", $sSentFrom = "", &$cOptions = array()){
		$cOptions['fileid'] = (int)$iFileId;
		return self::fnAddToFaxQueue("", $sSendTo, $sSentFrom, $cOptions);
	}
	

	/**
	* updates gps fax queue
	* @param int $iQueueId
	* @param array $cOptions name value pair of corresponding FaxInQueue
	* @return bool - true if data is saved
	* @see TSfax::fnInitFaxTables();
	*/
	public function fnUpdateFaxQueue($iFaxQueueId = 0, $cOptions = array()){
		$qSQL = "SHOW COLUMNS FROM `{$this->sFaxOutTable}` ";
		$result = $this->db->query($qSQL);
		$aColumns = array();
		if ($result) {
			while ($aRow = $this->fetch_assoc()) {
				$aColumns[$aRow['Field']] = true; 
			}
		}
		
		$cOptions = @array_intersect_key($cOptions, $aColumns); // this will only return array elements where $cOptions[$key] is a db column
		$aParams = array();
		foreach ($cOption as $field => $value) {
			$aParams[] = "`$field` = '" . $this->fnEscape($value) . "'";
		}
		if (count($aParams) > 0 && intval($iQueueId) != 0) {
			$qSQL = "UPDATE `{$this->sFaxOutTable}` SET " . join(', ', $aParams) . " WHERE FAXQUEUEID = " . $iFaxQueueId ;
			$this->db->query($qSQL);
			if ($this->db->errno) {
				return $this->fnSetFaxError($this->db->error);
			} else {
				return true;
			}			
		}

	}


	/**
	* sends fax (allows multiple faxes to single recipent)
	* #contacts array (name, recipeints,  array(tracking/ ref number, cover sheet)
	* send fax using the TSfax::fnOutboundFaxCreate method
	* @param mixed $faxes - paths where .pdf file is located
	* @param string $number - fax number destination
	* @param array $cOptions - reference array for input and output of data 
	* @see TSfas::cOptions
	* @return array - into the reference &$cOptions
	* @example "/var/www/sfax/examples.php" 2
	* 
	*/
	function fnSendFaxes($faxes = false, $number = "", &$cOptions = array()){
		if ($faxes) {
			$faxes = (is_array($faxes))  ? $faxes : array($faxes) ;
			$cOptions['faxids'] = array();
			foreach ($faxes as $key => $fax) {
				$faxNumber = $this->fnValidateFaxNumber($number);
				$faxFilePath = $this->fnValidateFaxPath($fax);
				if ($faxFilePath) {
					$faxFileType = $this->fnValidateFaxType($faxFilePath);
				}				
				if ($faxNumber && $faxFilePath && $faxFileType) {
					$faxid = $this->fnSendFax($faxFilePath, $faxNumber, $cOptions);
					
					if ($faxid) {
						$cOptions['faxids'][] = $this->fnGetFaxId();
						$cOptions['faxes'][$faxid] = $faxFilePath;
					} 			
				}			
			}
			$cOptions['messages'] = $this->fnGetFaxMessages();			
			// determines if all faxes were placed in queue
			return (count($faxes) == count($cOptions['faxids']))  ? true : false ;

		}

	}

	/**
	* send fax via sfax; creates report of activity
	* @param string $sFilePath - full path location of pdf/tif file to fax
	* @param int $sFaxNumber - destination of fax
	* @param array $cOptions - includes barcode information, recipients name
	* @return array - for the reference $cOptions return from TSFax::fnSendFax, TSFax::fnGetFax, and TSfax::fnGetFaxList
	* @uses TSfax::__call();
	*/
	public function fnSendFax($sFilePath, $sFaxNumber = "", $cOptions = array()){
		$sFaxRecipient = (isset($cOptions['name'])) ? $cOptions['name'] : $this->defaultName;
		$optionalParams						= $this->fnGetOptionsAsString();

		$aGetStrings						= array();
		$aGetStrings['token']				= $this->GenerateSecurityTokenUrl();
		$aGetStrings['ApiKey']				= $this->sApiKey;
		$aGetStrings['RecipientFax']		= $sFaxNumber;
		$aGetStrings['RecipientName']		= $sFaxRecipient;
		if ($optionalParams != "") {
			$aGetStrings['OptionalParams'] = $optionalParams;
		}

     
		if (!SEND_FAX) {
			echo "URL: " . ($this->cURL);
			return	;		
		}

		if (function_exists('curl_file_create')) {
			$aPostData['file']	= curl_file_create($sFilePath, 'application/pdf', basename($sFilePath));
		} else {
			$aPostData['file'] = '@' . $sFilePath;
		}
		

		$xResponse = $this->SendFax($aGetStrings, $aPostData);

		if ($this->aResponseInfo["http_code"] == 200) {
			$xResponseData = $this->GetResponseData($this->sResponseBody, $this->aResponseInfo);
            if ($xResponseData != null){
				$this->fnSetFaxSuccess("Fax successfully sent to " . $sFaxNumber);
				$this->fnSetFaxMessage("FaxQueueId: {$xResponseData->SendFaxQueueId}");
				$this->fnSetFaxId($xResponseData->SendFaxQueueId);
				return true;
            } else {
            	$this->fnSetFaxError("null response from curl operation");
				return false;
            }
		} else {
            $this->fnSetFaxError($this->aResponseInfo['http_code']);
			return false;
		}
		

	}
	

	/**
	* retreives fax via fnInboundFaxDownload
	* @param int $faxId
	* @param string $faxType - defaults to pdf
	* @param array $cOptions 
	* @return array $cOptions
	* @uses TSfax::__call();
	*
	*/
	public function fnGetFax($faxId, $faxType = 'pdf', &$cOptions = array()){
		$optionalParams						= $this->fnGetOptionsAsString();

		$aGetStrings						= array();
		$aGetStrings['token']				= $this->GenerateSecurityTokenUrl();
		$aGetStrings['ApiKey']				= $this->sApiKey;
		$aGetStrings['FaxId']				= $faxId;
		if ($optionalParams != "") {
			$aGetStrings['OptionalParams'] = $optionalParams;
		}
		$aPostData = array();
		$xResponse = $this->DownloadInboundFaxAsPDF($aGetStrings, $aPostData);

		if ($this->aResponseInfo["http_code"] == 200) {
			$afaxData = array('faxId' => $faxId, 'faxType' => $faxType);
			$this->fnWriteResponseToFile($this->sResponseBody, $this->aResponseInfo,  $afaxData);
			$cOptions = array_merge($cOptions, $afaxData);
			return true;
		} else {
            $this->fnSetFaxError($this->aResponseInfo['http_code']);
			if (!empty($error)) {
				$this->fnSetFaxError($error);
			}
			$cOptions = array_merge($cOptions, $this->fnGetFaxMessages());
			return false;			
		}

		return $cOptions;


	}
	





	/**
	* validates fax number
	* remove the number one in case it is sent with fax number
	* @param string $number
	* @return int|bool
	*/
	function fnValidateFaxNumber($number = ""){
		$regex = "/[^0-9]/";
		$faxNumber  = preg_replace($regex, "", $number);	 
		if (substr($faxNumber, 0, 1) == '1') {
			$faxNumber = substr($faxNumber, 1);
		}
		if (strlen($faxNumber) == 10) {
			return  '1' . $faxNumber;
		} else {
			$this->fnSetFaxError("Invalid Fax Number");
			return false;
		}
		 
	}

	/**
	* returns either a valid path to the file or false
	* @param string $sFilePath - full path to pdf file
	* @return bool|string
	*/
	function fnValidateFaxPath($sFilePath = ""){
		$sFilePath = str_replace("../", "", $sFilePath);
		if (is_file($sFilePath) && file_exists($sFilePath) && is_readable($sFilePath)) {
			return  $sFilePath;
		} else {
			$this->fnSetFaxError("Invalid or un-readable file path: {$sFilePath}"); 
			return false;
		}
		
	}


	/**
	* returns true on valid fax type using mime_content_type
	* @param string $sFilePath
	* @return bool
	*/
	function fnValidateFaxType($sFilePath = ""){
		if ($this->fnValidateFaxPath($sFilePath)) {
			$regex = "/\/tiff|\/pdf/";
			if (preg_match($regex, mime_content_type($sFilePath))) {
				return true;
			} else {
				$this->fnSetFaxError("Invaliad mime content type of file: {$filepath}");
				return false;
			}			
		} else {
			return false;
		}
	}


	/**
	* add valid options to be send with Sfax request
	* @param string $name
	* @param string $value
	* @return bool
	*/
	function fnAddOption($name, $value = ""){
		if ($value != "") {
			$this->options[$name] = $value;
			return true;
		} else {
			$this->fnSetError("an empty value for {$name} is not allowed");
			return false;
		}
	}


	/**
	* removes and existing option to be send with Sfax request
	* @param string $name
	* @return bool
	*/
	function fnRemoveOption($name = ""){
		if (in_array($name, $this->options)) {
			unset($this->options[$name]);
			return true;
		} else {
			$this->fnSetError("{$name} does not exists as an option");
			return false;
		}
	}

	/**
	* returns the set of options
	* @param void
	* @return array 
	*/
	function fnGetOptions(){
		return $this->options;
	}

	/**
	* returns the set of options as url string
	* @return string
	*/
	function fnGetOptionsAsString(){
		$options = $this->fnGetOptions();
		return (count($options) > 0 )  ? http_build_query($options, '', '&') : '' ;
	}

	/**
	* returns compile options (including sfax generated barcode)
	* @param int $pages
	* @param array $cOptions
	* @return string
	* @deprecated - since gps is creating its own barcode, this is not used
	*/
	public function fnSetOptions($pages = 1, $cOptions = array()){			
		$sBarcodeOptions = "";
		if (isset($cOptions['barcodeType']) && isset($cOptions['barcode'])) {
			$pages = (isset($cOptions['pages']))  ? intval($cOptions['pages']) : 0 ;
			$pages = $this->fnGetNumPages($sFilePath);
			$sBarcodeOptions = $this->fnSetBarcodeOptions($cOptions['barcodeType'], $pages, $cOptions['barcode']);
		}
		return $sBarcodeOptions;	
	}
	


	/**
	* adds an error message to the TSfax::$aMessages array 
	* @param string $sMessage
	* @return bool - always returns false
	*/
	function fnSetFaxError($sMessage = ""){
		self::fnSetFaxMessage('ERROR' , $sMessage);
		return false;
	}


	/**
	* adds a success message to the TSfax::$aMessages array 
	* @param string $sMessage
	* @return void
	*/
	function fnSetFaxSuccess($sMessage = ""){
		return self::fnSetFaxMessage('SUCCESS' , $sMessage);
	}

	/**
	* adds a message to the TSfax::$aMessages array 
	* @param string $sType
	* @param string $sMessage
	* @return void
	*/
	function fnSetFaxMessage($sType = "", $sMessage = ""){
		if ($sMessage != "") {			
			$this->aMessages[] = self::fnGetLocalTime() . " {$sType}: " . $sMessage;
		}		
	}


	/**
	* returns all fax messages
	* @param void
	* @return array|bool
	*/
	function fnGetFaxMessages(){
		if (!empty($this->aMessages)) {
			return $this->aMessages;
		} else {
			return false;
		}
	}


	/**
	* returns a formatted string for barcode production on sfax documents
	* @param string $barcodeType
	* @param int $pages (optional)
	* @param array|string $aBarcodes 
	* @return string|bool
	* @deprecated - barcodes to be createdy by GPS
	*/
	function fnSetBarcodeOptions($barcodeType, $pages = false, $aBarcodes = false){
		$aBarcodeSets = array(); 
		switch ($barcodeType) {
			case 'static': // unique barcode on each page ================================
				if ($aBarcodes && count($aBarcodes) > 0) {	
					foreach ($aBarcodes as $key => $aBarcode) {
						if (count($aBarcode) > 0) {
							$aBarcode = $this->fnSetBarcodeDefaults($aBarcode);							
							$aBarcodeSets[] = 'BarcodeOption=' . $this->fnSetNameValuePair($aBarcode, true, ';');

						}						
					}
					return join('&', $aBarcodeSets) . '&';
				}
			break; // ====================================================================
			
			case 'global': // same barcode on all pages
			case 'incremental': // same barcode with enumerated trailer
				// validate correct keys are within the aBarcodes array
				//$bValidKeys = count(array_intersect_key($aBarcodes, array_flip($this->aBarcodeElements))) > 0; 
				$bValidKeys = true;
				if ($bValidKeys) {
					if (intval($pages) > 0) {
						for ($i = 0; $i < $pages ; $i++) {
							$sBarcodeData = $aBarcodes; 
							$aIncrementalBarcodes = $this->fnSetBarcodeDefaults();
							$aIncrementalBarcodes['BarcodeData'] = ($barcodeType == 'incremental') ? $sBarcodeData . '_' . $i : $sBarcodeData;
							$aIncrementalBarcodes['BarcodePage'] = $i + 1;							
							$aBarcodeSets[] = 'BarcodeOption=' . $this->fnSetNameValuePair($aIncrementalBarcodes, true, ';');
							
						}
						return join('&', $aBarcodeSets);
					} else {
						$this->fnSetFaxError("invalid or missing page count or insufficent data");
					}					
				} else {
					$this->fnSetFaxError("Required barcode elements are missing");					
				}

			break;

			case 'none': // no barcode
				return '';
			break;

			default :
				$this->fnSetFaxError("Invalid Barcode Type");
				return ''; // no barcode data
			break;
		}
	}

	/**
	* sets build either an encoded or decode url string from an array
	* @param array $data
	* @param bool $encode
	* @param string $separator
	* @return string 
	*/
	function fnSetNameValuePair($data = array(), $encode = true, $separator = '&'){
		$params = array();
		foreach ((array)$data as $name => $value) {
			$params[] = ($encode)  ? "{$name}=" . urlencode($value) : "{$name}={$value}";
		}
		return  (count($params) > 0) ? join($separator, $params) : '' ;
	}
	
	

	/**
	* returns default values for information to create barcode 
	* in the event none are supplied
	* @param array $aBarcode
	* @return array
	*/
	function fnSetBarcodeDefaults($aBarcode = array()){
		$aDefaultBarcodeArray = array();
		foreach ($this->aBarcodeElements as $key => $value) {	
			$aDefaultBarcodeArray[$key] =(isset($aBarcode[$key]) && array_key_exists($key, $aBarcode))  ? $aBarcode[$key] : $value ;
		}		
		return $aDefaultBarcodeArray;
		
	}

	/**
	* returns number of pages of a given path to a pdf/tiff file
	*
	* @param sting $sFilePath
	* @return int|bool - TSfax::fnValidateFaxPath returns false
	*/
	function fnGetNumPages($filepath = "") {
		$fp = @fopen(preg_replace("/\[(.*?)\]/i", "", $filepath), "r");
		$numPages = 0;
		if (!$this->fnValidateFaxPath($filepath)) {
			return false;
		} else {
			$fp = @fopen($filepath, 'r');
			$numPages = 0;
			while (!@feof($fp)) {
				$line = @fgets($fp, 255);
				if (preg_match('/\/Count [0-9]+/', $line, $matches)) {
					preg_match('/[0-9]+/', $matches[0], $matches2);
					if ($numPages < $matches2[0]) {
						$numPages = trim($matches2[0]);
						break;
					}
				}
			}
			@fclose($fp);
		}

		return $numPages;
	}


	/**
	* returns xml from api sent to sfax
	* @param string $sAction - appended to endpoint url 
	* @param array $aData - 0) the get string to append to endpoint url; 1) post data, if any
	* @return array|bool
	*/
	public function __call($sAction, $aData = array()){
		$aPostData		= (isset($aData[1]))  ? $aData[1] : array();
		if (isset($aData[0])) {
			$aGetData		= $aData[0];
		} else {
			return $this->fnSetFaxError($sAction . ' contained no GET parameters, and thus could not initiate sfax api.' );
		}
		
		$sAction		= strtolower($sAction);
		if (is_array($aGetData) || is_object($aGetData)) {
			$this->cURL = $this->serviceEndpointUrl . $sAction . '?' . http_build_query($aGetData); 
		} else {
			echo '<pre><span style="color:red;">'; print_r($aGetData); echo '</span></pre>';
		}
		
		if (!SEND_FAX) {
			echo "URL: " . ($this->cURL);
			//return	;		
		}
		if (php_sapi_name() == 'cli') {
			$this->fnSetFaxSuccess($this->cURL);
		}

		$aCurlOptions							= array();
		$aCurlOptions[CURLOPT_URL]				= $this->cURL;
		$aCurlOptions[CURLOPT_HEADER]			= true;
		$aCurlOptions[CURLINFO_HEADER_OUT]		= true;
		$aCurlOptions[CURLOPT_NOBODY]			= false;
		$aCurlOptions[CURLOPT_RETURNTRANSFER]	= true;
		if ($aPostData && count($aPostData) > 0) {
			$aCurlOptions[CURLOPT_POST]				= true;
			$aCurlOptions[CURLOPT_POSTFIELDS]		= $aPostData;
		} else {
			$aCurlOptions[CURLOPT_POSTFIELDS]		= "";
			$aCurlOptions[CURLOPT_POST]				= false;

		}		
		if (isset($cOptions['secure']) && isset($cOptions['cert']) && @is_file($cOptions['cert'])) {
			$aCurlOptions = $this->fnAppendSecureConnect($cOptions['cert'], $aCurlOptions);
		} else {
			$aCurlOptions[CURLOPT_SSL_VERIFYPEER] = false;
		}

		$this->ch = curl_init();
		curl_setopt_array($this->ch, $aCurlOptions);


        $this->sResponseBody = curl_exec($this->ch);
        $this->aResponseInfo = curl_getinfo($this->ch);
        $error = curl_error($this->ch);
        curl_close ($this->ch);


		if ($this->aResponseInfo["http_code"] == 200) {
			$xResponseData = $this->GetResponseData($this->sResponseBody, $this->aResponseInfo);
            if ($xResponseData != null){
				//self::debug($xResponseData );
				if (isset($xResponseData->SendFaxQueueId)) {
					$this->fnSetFaxId($xResponseData->SendFaxQueueId);
				}
				$this->xReponseData = $xResponseData;
				if (isset($xResponseData->SendFaxQueueId)) {
					$this->fnSetFaxId($xResponseData->SendFaxQueueId);
				}				
				return $this->xReponseData;
            } else {
            	$this->fnSetFaxError("null response from curl operation");
				return false;
            }
		} else {
            $this->fnSetFaxError($this->aResponseInfo['http_code']);
			return false;
		}

		
	}
	



	/**
	* processes error response from sfax request
	* @param
	* @param
	* @return
	*/
	function fnGetErrorResponse($aResponseInfo = array()){
		return '';
	}



	/**
	* sets the value of the property TSfax::faxid
	* @param string $faxid
	* @return void
	*/
	function fnSetFaxId($faxid){
		if ($faxid != "") {
			$this->faxid = $faxid;
		}
	}

	/**
	* sets the value of the property TSfax::faxid
	* @param string $faxid
	* @return string|bool 
	*/
	function fnGetFaxId(){
		if (!empty($this->faxid)) {
			return $this->faxid;
		} else {
			return false;
		}
	}


	/**
	* sets formatted start date with setting date range for incoming faxes
	* @param string $sStartDate
	* @return void
	*/
	public function fnSetStartDate($sStartDate = ""){
		if (strtotime($sStartDate)) {
			$this->sStartDate = gmdate($this->sDateFormat, strtotime($sStartDate));
		} else {
			$this->sStartDate = gmdate($this->sDateFormat, time()); 
		}
	}

	/**
	* 
	* sets formatted end date with setting date range for incoming faxes
	* @param string $sEndDate
	* @return void
	*/
	public function fnSetEndDate($sEndDate = ""){
		if (strtotime($sEndDate)) {
			$this->sEndDate = gmdate($this->sDateFormat, strtotime($sEndDate));
		} else {
			$this->sEndDate = gmdate($this->sDateFormat, time()); 
		}		
	}

	/**
	* sets both start and end dates for incoming faxes
	* @param string $sStartDate
	* @param string $sEndDate
	* @return bool
	*/
	public function fnSetDates($sStartDate = "", $sEndDate = ""){
		$this->fnSetStartDate($sStartDate);
		$this->fnSetEndDate($sEndDate);
		return true;
	}

	/**
	* sets max item retreival for incoming faxes
	* @param int $iMaxItems
	* @return void
	*/
	public function fnSetMaxItems($iMaxItems = 500){
		$this->max_items = (intval($iMaxItems) != 0)  ? $iMaxItems : 500 ;
	}

	/**
	* sets max item retreival for incoming faxes default set to 24 hours prior
	* @param int $iMaxItems
	* @return void
	*/
	public function fnSetWatemarkId($iWatermarkId = 0){
		/*
		from the sfax documentation:
		Watermarks are formatted by date with a 12 digit unique id on the end. For example: 
		2140304211245997698. The 214 represents 2014, 03 is the month, 04 is the day and 211245997698 is the unique id. 
		So, if you want to reset back to Feb 25, 2013 you can enter the date and then 12 zeroes on the end for the id.  For example: 2130225000000000000.		
		*/
		$yesterday = strtotime('yesterday');
		$this->WatermarkId = (intval($iWatermarkId) != 0)  
			? $iWatermarkId 
			: substr(date('Y', $yesterday), 1) . date('md', $yesterday) . str_repeat('0', 12);
		return $this->WatermarkId;
	}

	/**
	* returns the watermarkid from the last record in the specific db
	* if none exists, and default watermark id is created
	* @param string $sDBTable - either the fax in or fax out table
	* @return int
	*/
	public function fnGetLastWatemark($sTableType = "in"){
		$sDBTable = ($sTableType == 'in')  ? $this->sFaxInTable : $this->sFaxOutTable  ;
		$autoId = strtoupper(str_replace('client_', '', $sDBTable)) . 'ID';
		$qSQL = "SELECT WATERMARKID FROM $sDBTable WHERE BDOWNLOADED = 1 ORDER BY DATEMODIFED DESC, {$autoId} DESC LIMIT 1";
		$result = $this->db->query($sql);
		if ($result) {
			while ($row = $db->fetch_assoc()) {
				if (isset($row['WATERMARKID'])) {
					return $row['WATERMARKID'];
				}
			}
		}
		return $this->fnSetWaterMarkId();

	}

	/**
	* adds additional curl options to the curl request if a secure certificate is available
	* @param string $cert_path
	* @return void
	* @deprecated used for now use TSfax::fnAppendSecureConnect() for now
	*/
	function fnSecureConnect($cert_path = ""){
		if (is_file($cert_path) && is_readable($cert_path)) {
			curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($this->ch, CURLOPT_CAINFO, $cert_path);			
		} else {
			curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		}
	}


	/**
	* adds additional curl options to the curl request if a secure certificate is available
	* @param string $sCertificatePath
	* @param array $aCurlOptions - existing curl coptions
	* @return array 
	*/
	function fnAppendSecureConnect($sCertificatePath = "", $aCurlOptions = array()){
		if (is_file($sCertificatePath) && is_readable($sCertificatePath)) {
			$aCurlOptions[CURLOPT_SSL_VERIFYPEER] = true;
			$aCurlOptions[CURLOPT_SSL_VERIFYHOST] = 2;
			$aCurlOptions[CURLOPT_CAINFO] = $sCertificatePath;
		} else {			
			$aCurlOptions[CURLOPT_SSL_VERIFYPEER] = false; 
		}
		return $aCurlOptions;
	}



// methods taken fron TFTSHelper.php ===================================================================

	/**
	* returns headers from curl response
	* @param string $this->sResponseBody
	* @param array $this->aResponseInfo
	* @return array
	*/
    public static function GetHeaders($sResponseBody, $aResponseInfo){
        $header_text = substr($sResponseBody, 0, $aResponseInfo['header_size']);
        $headers = array();
        foreach(explode("\n",$header_text) as $line){
            $parts = explode(": ",$line);
            if(count($parts) == 2){
                if (isset($headers[$parts[0]])){
                    if (is_array($headers[$parts[0]])) $headers[$parts[0]][] = chop($parts[1]);
                    else $headers[$parts[0]] = array($headers[$parts[0]], chop($parts[1]));
                } else {
                    $headers[$parts[0]] = chop($parts[1]);
                }
            }
        }
        return $headers;        
    }	


	/**
	* returns data from curl response for outbound fax
	* @param string $sResponseBody
	* @param array $aResponseInfo
	* @uses TSfax::$Messages
	* @return array
	*/
	public static function GetResponseData($sResponseBody, $aResponseInfo){
        $body = "" . substr($sResponseBody, $aResponseInfo['header_size']);        
		return json_decode($body);
	}

	/**
	* returns data from curl response for inbound fax
	* @param string $sResponseBody
	* @param array $aResponseInfo
	* @uses TSfax::$Messages
	* @return array
	*/
	public static function GetInboundResponseData($sResponseBody, $aResponseInfo){
        $body = "" . substr($sResponseBody, $aResponseInfo['header_size']);
		return json_decode($body);
        echo "InboundRetrieveSets: " . $body;		
	}


	/**
	* returns data from curl response for outbound fax
	* @param string $sResponseBody
	* @param array $aResponseInfo
	* @param array aFaxData  - faxid and file type
	* @uses TSfax::$Messages
	* @return bool
	* @deprecated
	*/
	public function fnWriteResponseToFile($sResponseBody, $aResponseInfo, $aFaxData = array()){
		
		echo self::debug($aFaxData);

		if (!isset($aFaxData['faxId']) || !isset($aFaxData['faxType'])) {
			return false;
		}
		
		$sFileData = substr($sResponseBody, $aResponseInfo['header_size']);

		if (!class_exists('TRemoteFileSystem')) {
			/*
			testing the fax dump in a temp location			
			*/
			$sFilePath =  $this->sInboundFaxPath . $aFaxData['faxId'] . "." . $aFaxData['faxType'];
			return $this->fnSaveDownloadedFax($sFileData, $sFilePath);
		} else {
			$this->db = new mysqli(_DBSERVER_, _DBUSER_, _DBPWD_, _DBNAME_);	
			
			$clientid = FN_CLIENTID();
			$volume = $clientid . '/' . $this->storepath;

			$sql = "INSERT INTO `gps_files` (CLIENTID, VOLUMEID, FILENAME, FILEDATE, SAVEDATE, FILESIZE, STORETYPE, STOREPATH) ";
			$sql .="VALUES('{$clientid}', '{$clientid}', '{$localFileName}', '" . $this->fnGetLocalTime() . "', '" . $this->fnGetLocalTime() . "', '" . strlen($sFileData) . "', '0', '{$volume}')";
			
			echo self::debug($sql);

			if ($this->db->query($sql)) {
				$resourceId		= $this->db->insert_id;
				$aOptions		= array('volume' => $volume);
				// add other $aOptions here
				
				$oFile			= new TRemoteFileSystem();
				$bSaved			= $oFile->fnPutFile($resourceId, $sFileData, $aOptions);
				if ($bSaved) {
					// update gps_callback info
					$sql = "UPDATE `gps_sfaxcallback` SET BDOWNLOADED = 1, DOWNLOADEDDATE = '" . $this->fnGetLocalTime() . "' WHERE FAXID = " . intval($faxId) . " LIMIT 1";
					$this->db->query($sql);
					return true;
				} else {
					$aMessages = $oFile->fnGetFileMessages();
					if ($aMessages && count($aMessages) > 0) {
						foreach ($aMessages as $key => $message) {
							$this->fnSetFaxError($message);
						}					
					}
					return false;
				}

			} else {
				$this->fnSetFaxError($this->db->error);
				return false;
			}			
		}
		

	}



	/**
	* If no access to remote file system
	* we used this method to save fax as media for review
	* @param string $sFileData - input data for file
	* @param string $sFilePath - location of file to be stored
	* @return bool
	*/
	function fnSaveDownloadedFax($sFileData = "", $sFilePath = ""){
		$bSaved = false;
		if (!is_writeable(dirname($sFilePath))) {
			$this->fnSetFaxError("The path to {$sFilePath} is not writeable.");
			$bSaved = false;
		} elseif (strlen($sFileData) == 0) {
			$this->fnSetFaxError("There is no data to write to {$sFilePath}.");
			$bSaved = false;
		} else {
			$fh = @fopen($sFilePath, "w");
			$iBytesWritten = @fwrite($fh, $sFileData);
			if ($iBytesWritten != strlen($sFileData)) {
				$this->fnSetFaxError("The number of bytes written to {$sFilePath}. does not match the data size.");
				$bSaved = false;
			} else {
				fclose($fh);
				$this->fnSetFaxSuccess("Create Fax is located at: {$sFilePath}");
				$bSaved = true;
			}
		}
		return $bSaved;
	}




// ========================================================================================================

	/** 
	* creates a file from the remote file system; returns full path of that file
	* @param int $fileId - file Id located in gps_files
	* @param string $sTempFileName - optional name of the file to be stored in the temp location
	* @return string|bool - false if file is not found; else the full path of the created file
	*/
	public function fnGetFileById($fileId = "", $sTempFileName = ""){
		
	}
	

// getters and setters api configuration for multiple accounts ===========================================

	/**
	* sets api key - used for other accounts
	* @param string $sApiKey
	* @return bool
	*/
	public function fnSetApiKey($sApiKey = ""){
		if ($sApiKey != "") {
			$this->sApiKey = $sApiKey;
			return true;
		} else {
			return false;
		}
	}


	/**
	* sets username - used for other accounts
	* @param string $sApiKey
	* @return bool
	*/
	public function fnSetUserName($sUsername = ""){
		if ($sUsername != "") {
			$this->pTokenUsername = $sUsername;
			$this->sUsername = $sUsername;
			return true;
		} else {
			return false;
		}
	}

	
	/**
	* sets api key - used for other accounts
	* @param string $pEncryptionKey
	* @return bool
	*/
	public function fnSetEncryptionKey($pEncryptionKey = ""){
		if ($pEncryptionKey != "") {
			$this->pEncryptionKey = $pEncryptionKey;
			return true;
		} else {
			return false;
		}		
	}
	
	/**
	* sets api key - used for other accounts
	* @param string $sApiKey
	* @return bool
	*/
	public function fnSetInitVector($pEncryptionInitVector = ""){
		if ($pEncryptionInitVector != "") {
			$this->pEncryptionInitVector = $pEncryptionInitVector;
			return true;
		} else {
			return false;
		}		
	}
	
	/**
	* returns inboud fax path
	* @param
	* @param
	* @return string
	*/
	public function fnGetInboundFaxPath($bFull = true){
		if (isset($this->sInboundFaxPath)) {
			return ($bFull)  ? $this->sInboundFaxPath : str_replace($this->sRootPath, '', $this->sInboundFaxPath);
		}		
	}
	
	/**
	* sets inboud fax path removes root path and any relative paths
	* @param string $sInboundFaxPath 
	* @return bool - false if $sInboundFaxPath is not a real director
	*/
	public function fnSetInboundFaxPath($sInboundFaxPath = ""){
		if (is_dir($sInboundFaxPath)) {
			$this->sInboundFaxPath =   $this->sRootPath . basename($this->sInboundFaxPath);
		} else {
			return false;
		}		
	}


// =======================================================================================================




// methods taken from TFTSAESHelper.php ===================================================================


	/**
	* generates token url using various properties
	* @param void
	* @return string
	* @uses TAES_Encryption
	*/
    public function GenerateSecurityTokenUrl(){
        $tokenDataInput;
        $tokenDataEncoded;
        $tokenGenDT;
        $tokenGenDT = gmdate("Y-m-d") . "T" . gmdate("H:i:s") . "Z";
        $tokenDataInput = "Context=" . $this->pTokenContext . "&Username=" . $this->pTokenUsername. "&ApiKey=" . $this->sApiKey . "&GenDT=" . $tokenGenDT . "";
        if($this->pTokenClient != null && $this->pTokenClient != ""){
            $tokenDataInput .= "&Client=" . $this->pTokenClient;
        }
		//echo self::debug(debug_backtrace());// print_r(debug_backtrace());
        $AES = new TAES_Encryption($this->pEncryptionKey, $this->pEncryptionInitVector, "PKCS7", "cbc");
        $tokenDataEncoded = base64_encode($AES->encrypt($tokenDataInput));
        return $tokenDataEncoded;
    }	


// =======================================================================================================

	/**
	* returns local time in given format
	* @param string $format
	* @return string - local time
	*/
	function fnGetLocalTime($format = 'Y-m-d H:i:s'){
		$date = new DateTime('now', new DateTimeZone('America/New_York'));
		return $date->format($format);
	}


	/**
	* returns total time expired until this method is called
 	* @param void
	* @return float
	*
	*/
	function fnGetTotalTime(){
		return number_format(array_sum($this->rawtimes), $this->decimalplace);
	}




	/**
	* placeholder method to execute query, or display query in a non-live mode
	* @param string $sql
	* @return bool
	*/
	function fnExecQuery($sql){
		if (!$this->db->query($sql)) {
			self::log($this->db->error . "\n sql: {$sql}");
			$this->sError .= $this->db->error . "\n sql: {$sql}"; //mysql_error($this->db);			
			return false;				
		} else {
			return true;
		}
	}

 	/**
	* returns escaped value for db insertion; recursively if an array
	* using mysqli package
	* @param mixed - can be either string or array
	* @return array|string|bool
	*/
	public function fnEscape($mData = false){
		if (!is_object($mData)) {
			return (!is_array($mData))  ? $this->db->real_escape_string($mData) : array_map(array($this, 'fnEscape'), $mData) ;
		} else {
			return $this->fnSetFaxError("Input data is not a value string or array");
		}
	}
	


	/**
	* returns creation query for client_faxqueue table
	* @param void
	* @return string
	*/
	public function fnInitSfaxOutTable(){
		return "
		CREATE TABLE `client_faxqueue` (
		`FAXQUEUEID` INT(10) NOT NULL AUTO_INCREMENT,
		`CLIENTID` INT(4) NULL DEFAULT NULL,
		`FILEID` INT(10) NULL DEFAULT NULL,
		`SENDFAXQUEUEID` VARCHAR(50) NULL DEFAULT NULL,
		`FAXID` VARCHAR(50) NULL DEFAULT NULL,
		`BINPROGRESS` TINYINT(4) NULL DEFAULT '0',
		`TOFAXNUMBER` VARCHAR(50) NULL DEFAULT NULL,
		`FROMFAXNUMBER` VARCHAR(50) NULL DEFAULT NULL,
		`RECIPIENTNAME` VARCHAR(80) NULL DEFAULT NULL,
		`ERRORCODE` INT(11) NULL DEFAULT NULL,
		`RESULTMESSAGE` VARCHAR(50) NULL DEFAULT NULL,
		`ATTEMPTS` VARCHAR(50) NULL DEFAULT NULL,
		`FROMCSID` VARCHAR(50) NULL DEFAULT NULL,
		`CREATEDATEISO` CHAR(50) NULL DEFAULT NULL,
		`DATECREATED` DATETIME NULL DEFAULT NULL,
		`FAXDATETIME` DATETIME NULL DEFAULT NULL,
		`FAXDATEISO` CHAR(50) NULL DEFAULT NULL,
		`BARCODES` TEXT NULL,
		`BARCODEDATA` TEXT NULL COMMENT 'serialized data of barcode information',
		`FAXPAGES` INT(11) NULL DEFAULT NULL,
		`WATERMARKID` VARCHAR(50) NULL DEFAULT NULL,
		`BUPLOADED` TINYINT(1) NULL DEFAULT '0',
		`BSENT` TINYINT(1) NULL DEFAULT '0',
		`SENTTIME` DATETIME NULL DEFAULT NULL,
		`UPLOADEDEDDATE` DATETIME NULL DEFAULT NULL,
		`FAXDATA` TEXT NULL COMMENT 'serialized data for the fax',
		`FILEPATH` TEXT NULL,
		`FILESIZE` INT(10) NULL DEFAULT '0',
		`DATEMODIFIED` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`FAXQUEUEID`),
		INDEX `CLIENTID` (`CLIENTID`),
		INDEX `FAXID` (`SENDFAXQUEUEID`),
		INDEX `FILEID` (`FILEID`),
		INDEX `SENDFAXQUEUEID` (`FAXID`),
		INDEX `BINPROGRESS` (`BINPROGRESS`)
		)
		COLLATE='latin1_swedish_ci'
		ENGINE=InnoDB
		ROW_FORMAT=COMPACT
		;

		";
	}
	

	/**
	* returns creation query for client_sfaxout table
	* @param void
	* @return string
	*/
	public function fnInitSfaxInTable(){
			return "
			CREATE TABLE `client_faxin` (
			`FAXINID` INT(10) NOT NULL AUTO_INCREMENT,
			`CLIENTID` INT(4) NULL DEFAULT NULL,
			`FAXID` VARCHAR(50) NULL DEFAULT NULL,
			`TOFAXNUMBER` VARCHAR(50) NULL DEFAULT NULL,
			`FROMFAXNUMBER` VARCHAR(50) NULL DEFAULT NULL,
			`FROMCSID` VARCHAR(50) NULL DEFAULT NULL,
			`CREATEDATEISO` CHAR(50) NULL DEFAULT NULL,
			`DATECREATED` DATETIME NULL DEFAULT NULL,
			`FAXDATETIME` DATETIME NULL DEFAULT NULL,
			`FAXDATEISO` CHAR(50) NULL DEFAULT NULL,
			`BARCODES` TEXT NULL,
			`BARCODEDATA` TEXT NULL COMMENT 'serialized data of barcode information',
			`FAXPAGES` INT(11) NULL DEFAULT NULL,
			`WATERMARKID` VARCHAR(50) NULL DEFAULT NULL,
			`BDOWNLOADED` TINYINT(1) NULL DEFAULT '0',
			`BSAVED` TINYINT(1) NULL DEFAULT '0',
			`DOWNLOADEDDATE` DATETIME NULL DEFAULT NULL,
			`FAXDATA` TEXT NULL COMMENT 'serialized data for the fax',
			`FILEPATH` TEXT NULL,
			`FILESIZE` INT(10) NULL DEFAULT '0',
			`DATEMODIFIED` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`FAXINID`),
			INDEX `CLIENTID` (`CLIENTID`),
			INDEX `FAXID` (`FAXID`)
			)
			COLLATE='latin1_swedish_ci'
			ENGINE=InnoDB
			;
			";
	}
	


	/**
	* return pre-formatter debugging output
	* @param void
	* @uses $this
	* @return string
	*/
	function __toString(){
		return self::debug($this);
	}

	/**
	* for debugging only
	* outputs function arguments to string
	*/	 	 
    function debug(){	
		ob_start();
			$args = func_get_args();
			foreach ((array) $args as $anything) {
				if(is_array($anything) || is_object($anything)){
					echo '<pre>';
					print_r($anything);
					echo '</pre>';					
				} else {
					echo '<pre>';
					var_dump($anything);
					echo '</pre>';
					
				}
			}
		return ob_get_clean(); 
    }

	/**
	* output debugger to error log
	*/
	function log(){
		error_log(self::debug());	
	}


	/**
	* class destructor - return eviroment varaibles to orginal state
	* @return void
	*/
	function __destruct(){
		set_time_limit($this->iTimeLimit);
		ini_set('memory_limit', $this->sMemoryLimit);
	}
}