<?php
/**
* This class handles incoming faxes
* 
*
*/

/**
* A child of TSfax - designed to work from browser or command line
* 
* @author J.Johnson <jj@elgonsystems.com>*
* @package sfax
* @since 2016-08-05
* @version 1.0
* @copyright 2016 Elgon Information Systems, LLC
*
* 
*/

require_once(__DIR__ . '/TSfax.php');
require_once(__DIR__ . '/TAES_Encryption.php');	

class TSFaxIncomingCron extends TSfax
{

	/**
	*
	* @author J.Johnson <jj@elgonsystems.com>
	* @package sfax
	*
	*/
	
	/**
	* class constructor - initiates stand-alone db connection
	* parent constructor loads api key
	*/
	public function __construct(){
		parent::__construct();

	

		
		$this->sInboundFaxPath			= $this->tempDir . '/incoming/';

		$this->fnSetEndDate();
		$this->fnSetStartDate(date('m/d/Y', (strtotime($this->sEndDate) - 86400))); // default start date set to 24 hours prior to end date
		$this->iMaxItems = 500;
		//$this->fnSetWatermarkId();
		$this->fnSetFaxSuccess('Start Incoming Fax Process');
	
	}



	/**
	* retreives fax via fnInboundRetrieveFaxSet
	* api key and other tokens should be changed before calling this class
	* @param array $cOptions include start dates ('StartDateUTC'), end dates('EndDateUTC'); max items ('MaxItems')
	* @return array
	* @see TSfax::fnSetStartDate()
	* @see TSfax::fnSetEndDate()
	* @see TSfax::fnSetDates()
	* @see TSfax::fnMaxItems()

	*
	*/
	function fnGetFaxes( &$cOptions = array()){

		$aAdditionParams	= array(
			'StartDateUTC'	=> 'sStartDate', 
			'EndDateUTC'	=> 'sEndDate', 
			'MaxItems'		=> 'iMaxItems', 
			//'WatermarkId'	=> 'sWatermarkId'
		);
		$aGetStrings						= array();
		$aGetStrings['token']				= $this->GenerateSecurityTokenUrl();
		$aGetStrings['ApiKey']				= $this->sApiKey;
		
		foreach ($aAdditionParams as $sKey => $sParam) {
			if (array_key_exists($sKey, $cOptions)) {
				$aGetStrings[$sKey] = $cOptions[$sKey]; 
			} else {
				$aGetStrings[$sKey] = $this->$sParam;
			}
		}

		// should we add a method here to fetch the last watermarkid from the db 
		// instead of using the default date range of the last 24 hours?

		if (!GET_FAX) {
			echo "URL: " . ($this->cURL);
			return	;		
		}

		
		$xResponse = $this->ReceiveInboundFax($aGetStrings);
		if ($xResponse) {
			$this->fnSetFaxSuccess("Faxes Detected From Sfax API: " . $xResponse->FaxCount);
		}		
		$bHasMoreItems = true;
		do {
			$xResponse = $this->ReceiveInboundFax($aGetStrings);

			// in case of api error, the api response should pick up right where it left off
			// needs serious unit testing
			//if (isset($xResponse->LastWatermark)) {
				//$aGetString['WatermarkId'] = $xResponse->LastWatermark;
			//}
			// =============================================================================

			if (!$xResponse) {
				return;
			} else {
				$aIncomingFaxes = $xResponse->InboundFaxItems;
				$bHasMoreItems = ($xResponse->HasMoreItems === true) ? true: false ;
				foreach ((array)$aIncomingFaxes as $key => $oIncomingFax) {

					// check db for fax record of download; if noe exsists, create one
					// return added info to existing object ($IncomingFax)
					$oIncomingFax = $this->fnCreateFaxRecord($oIncomingFax); 

					if (!$oIncomingFax->bDownloaded) {

						// download fax (and/or) write to remote system
						// return added info to existing object ($IncomingFax)
						$oIncomingFax = $this->fnDownloadFax($oIncomingFax);
						if ($oIncomingFax->bSaved) {
							$this->fnUpdateFaxRecord($oIncomingFax); // updates record in db
						}
					}
				}
			}
		} while ($bHasMoreItems);
		$this->fnSetFaxSuccess('End Incoming Fax Process');
		$cOptions['messages'] = $this->fnGetFaxMessages();
		//echo self::debug($xResponse);
		return $cOptions;

	}

	


	/**
	* processes fax object from Sfax api response; 
	* returns additional properties to object based on db criteria
	* @param object $oFaxObject
	* @return object
	*/
	public function fnCreateFaxRecord($oIncomingFax = null){
		$oIncomingFax->bDownloaded = false;
		$qSQL = "SELECT * FROM $this->sFaxInTable WHERE `FAXID` = '" . $this->db->real_escape_string($oIncomingFax->FaxId) . "'";
		$result = $this->db->query($qSQL);
		if (!$result) {
			return $this->fnSetFaxErrorMessage($this->db->error);
		}
		if ($result->num_rows == 0) {
			$sql = "SHOW COLUMNS FROM " . $this->sFaxInTable;
			$result = $this->db->query($sql);			
			$aFields = array();
			while ($row = $result->fetch_assoc()) {					
				$aFields[] = $row['Field'];
			}	
			$aParams = array();				
			foreach ((array)$oIncomingFax as $sField => $sValue) { 
				if (in_array(strtoupper($sField), $aFields) && !empty($sValue)) {
					if (method_exists($this, $sField)) { // used to add elements to the array that sfax does not send
						$aParams = $this->$sField($aParams, $sValue);
					} else {						
						$aParams[] = "`" .  strtoupper($sField) . "` = '" . $this->db->real_escape_string($sValue) . "'";						
					}
				}
			}

			if (count($aParams) > 0) {
				$aParams[] = "`FAXDATA` = '" . serialize($oIncomingFax) . "'"; // grabs the entire record of data
				$qSQL = "INSERT INTO {$this->sFaxInTable} SET " . join(", ", $aParams);
				$this->db->query($qSQL);				
				if ($this->db->errno) {					
					return $this->fnSetFaxError($this->db->errno . ' ' . $qSQL);
				} else {
					$this->fnSetFaxSuccess("Record Created: " . $qSQL);				
				}
			}					
		} else {
			// there is a record; add more properties to $oIncomingFax object
			while ($aRow = $result->fetch_assoc()) {
				$oIncomingFax->bDownloaded = ($aRow['BDOWNLOADED'] == 1 && $aRow['BSAVED'] == 1)  ? true : false ;
			}
		}
		return $oIncomingFax;
	}




	/**
	* downloads fax pdf to web server from sfax api
	* @param object $oIncomingFax 
	* @return object
	*
	*/
	public function fnDownloadFax($oIncomingFax = false){
		if ($oIncomingFax && gettype($oIncomingFax) == 'object') {
			$aGetStrings						= array();
			$aGetStrings['token']				= $this->GenerateSecurityTokenUrl();
			$aGetStrings['ApiKey']				= $this->sApiKey;
			$aGetStrings['FaxId']				= $oIncomingFax->FaxId;	

			$aPostData = array();
			$xResponse = $this->DownloadInboundFaxAsPDF($aGetStrings, $aPostData);
			if ($this->aResponseInfo["http_code"] == 200) {

				$sFileData = substr($this->sResponseBody, $this->aResponseInfo['header_size']);
				$oIncomingFax->sFilePath =  $this->sInboundFaxPath . $oIncomingFax->FaxId . ".pdf";
				if ($this->fnSaveDownloadedFax($sFileData, $oIncomingFax->sFilePath)) {
					$oIncomingFax->iFileSize = strlen($sFileData);
					$oIncomingFax->bSaved = true;
				} else {
					$oIncomingFax->bSaved = false;
					$oIncomingFax->iFileSize = 0;
				}
			} else {
				$this->fnSetFaxError("Invalid or missing fax object");
			}
			
		}
		return $oIncomingFax;		
	}



	/**
	* update fax record of incoming fax
	* @param object $oIncomingFax 
	* @return object
	*
	*/
	public function fnUpdateFaxRecord($oIncomingFax = false){
		if ($oIncomingFax && gettype($oIncomingFax) == 'object') {
			$qSQL = "
			UPDATE {$this->sFaxInTable} 
			SET `BDOWNLOADED` = '1', 
			`FILEPATH` = '{$oIncomingFax->sFilePath}', 
			`FILESIZE` = '{$oIncomingFax->iFileSize}', 
			`DOWNLOADEDDATE` = '" . $this->fnGetLocalTime() . "',
			`BSAVED` = '1'
			WHERE FAXID = '{$oIncomingFax->FaxId}'";
			$this->fnSetMessage('UPDATED: ' . $qSQL);
			$this->db->query($qSQL);

		}
		if ($this->db->error) {
			$oIncomingFax->bCompleted = false;
			$this->fnSetFaxError($this->db->error);
		} else {
			$oIncomingFax->bCompleted = true;
			$this->fnSetFaxSuccess("Record for FaxID# " . $oIncomingFax->FaxId . " has been updated.");			
		}
		return $oIncomingFax;		
	}




	/**
	* converts faxdateiso to proper time; adds to params array
	* @param array $aParams
	* @param string $faxdateiso
	* @return array
	*/
	function faxdateiso($aParams, $faxdateiso = ""){
		if (strtotime($faxdateiso)) {
			$date = new DateTime($faxdateiso);
			$date->setTimezone(new DateTimeZone('America/New_York'));
			$aParams[]  = "`FAXDATETIME` = '" . $date->format('Y-m-d H:i:s') . "'";
			$aParams[]  = "`FAXDATEISO` = '" . $this->db->real_escape_string($faxdateiso) . "'";

		}
		return $aParams;
	}


	/**
	* converts faxdate to proper time; adds to params array
	* @param array $aParams
	* @param string $faxdateiso
	* @return array
	*/
	function faxdate($aParams, $faxdateiso = ""){
		if (strtotime($faxdateiso)) {
			$aParams[]  = "`FAXDATEISO` = '" . date('Y-m-d H:i:s', strtotime($faxdateiso)) . "'";
		}
		return $aParams;
	}
	/**
	* returns client id
	* @param array $aParams
	* @param string $faxid
	* @return array
	* @todo - get client id from global source
	*/
	function faxid($aParams, $faxId = ""){
		if ($faxId != "") {
			$aParams[] = "`CLIENTID` = " . $this->clientId;
			$aParams[] = "`FAXID` = '" . $this->db->real_escape_string($faxId) . "'";			
		}
		return $aParams;
	}


	/**
	* adds addition flag for fax success
	* @param array $aParams
	* @param string $faxsuccess
	* @return array
	*/
	function faxsuccess($aParams, $faxsuccess = ""){
		if ($faxsuccess != "") {
			$aParams[] = "`BFAXSUCCESS` = '" . $this->db->real_escape_string($faxsuccess) . "'";			
		}
		return $aParams;		
	}

	/**
	* serializes the incoming barcode data
	* @param array $aParams
	* @param object $oBarcodes
	* @return array 
	*/
	public function barcodes($aParams, $oBarcodes){
		$aParams[] = "`BARCODEDATA` = '" . $this->db->real_escape_string(serialize($oBarcodes)) . "'";
		$aParams[] = "`BARCODES` = '" . $this->db->real_escape_string(serialize($oBarcodes->BarcodeItems)) . "'"; 
		return $aParams;
	}


	/**
	* re-formats the incoming iso date
	* @param array $aParams
	* @param string dtCreateDateIso
	* @return array 
	*/
	public function createdateiso($aParams, $dtCreateDateIso){
		$aParams[] = "`CREATEDATEISO` = '" . $this->db->real_escape_string($dtCreateDateIso) . "'";
		$aParams[] = "`DATECREATED` = '" . date('Y-m-d H:i:s', strtotime($dtCreateDateIso)) . "'";
		return $aParams;
	}
	/**
	* class destructor
	* @param void
	* @return void
	*/
	public function __destruct (){
		$this->fnSetFaxSuccess('End Incoming Fax Process');
		parent::__destruct();
	}
	


}


