<?php
/**
* file for outgoing faxes
* 
*
*/

/**
* class_description
* this will server as a gps class template
* 
* @author J.Johnson <jj@elgonsystems.com>*
* @package package
* @since
* @version
* @copyright
*
* 
*/


require_once(__DIR__ . '/TSfax.php');


class TSfaxOutgoingCron extends TSfax
{
	

	/**
	* @var array - references array 
	* @access protected
	*/

	protected $cOptions;


	/**
	* @var float - start at time 0 for benchmark purposes
	* @access private
	*/

	private $time_start;

	/**
	* @var array - array of benchmarks logs within class
	* @access private
	*/

	private $timeline = array();

	/**
	* @var int - number of decimal places for benchmark logs
	* @access private
	*/

	private $decimalplace = 7;

	/**
	* @var type - description
	* @access access
	*/

	/**
	* class constructor
	*
	* @param
	* @param
	* @return
	*/
	public function __construct(){
		parent::__construct();

		// benchmark properties
		$this->time_start	= microtime(true);
		$this->timeline		= array();
		$this->fnSetFaxSuccess('Start Outgoing Fax Process');


	}

		
	/**
	* runs via cron: upload awaiting faxes to sfax
	* @param
	* @param void
	* @return array TSfax::aMessages();
	* @uses TSfax::fnSendFaxes()
	*/
	public function fnCronSendFaxes(&$cOptions = array()){

		if ($this->fnIsAtCapacity()) {
			return $this->fnSetFaxError("Awaiting Faxes have reached their limit in queue. Try again later.");			
		}

		$qSQL = "SELECT * FROM {$this->sFaxOutTable} WHERE BUPLOADED != 1 AND BINPROGRESS = 0 LIMIT {$this->iFaxQueueLimit}"; // and BINPROGRESS=0
		$result = $this->db->query($qSQL);
		$aFaxesInQueue = array();
		if ($result) {
			while ($aRow = $result->fetch_assoc()) {
				$aFaxesInQueue[] = $aRow; 
			}
			if (count($aFaxesInQueue) > 0) {
				foreach ((array)$aFaxesInQueue as $key => $aFaxInQueue) {
					if (!empty($aFaxInQueue['FILEID'])) {
						// we must fetch the file from the remote file system and 
						// save it to the outbound folder
						if (class_exists('TRemoteFileSystem')) {
							$rfs = new TRemoteFileSystem();
							$sFilePath = $this->sOutboundFaxPath . $aFaxInQueue['FILEID'] . '.pdf';
							$sFileData = $rfs->fnGetFile($aFaxInQueue['FILEID']);
							$bSaved = $this->fnSaveDownloadedFax($sFileData, $sFilePath);
							if ($bSaved) {
								$aFaxInQueue['FILEPATH'] = $sFilePath;
								$aFaxInQueue['FILESIZE'] = strlen($sFileData);
							}						
						} else {
							$this->fnSetFaxError("Unable to reach Remote File ID# " . $aFaxInQueue['FILEID']);
						}
					} else {
						$sFileData = $this->sOutboundFaxPath . $aFaxInQueue['FILEPATH'];
						$aFaxInQueue['FILEPATH'] = $sFileData;
						$aFaxInQueue['FILESIZE'] = strlen($sFileData);					
					}
					// set flag to in progress (in case another session is currently running)
					$qSQL = "UPDATE {$this->sFaxOutTable} SET BINPROGRESS = 1 WHERE FAXQUEUEID = '{$aFaxInQueue['FAXQUEUEID']}'";
					$this->db->query($qSQL);

					// transfer fax to the remote sfax queue here:					
					$bUploaded = $this->fnSendFaxes($aFaxInQueue['FILEPATH'], $aFaxInQueue['TOFAXNUMBER'], $cOptions);

					// set flag to not in progress
					$qSQL = "UPDATE {$this->sFaxOutTable} SET BINPROGRESS = 0 WHERE FAXQUEUEID = '{$aFaxInQueue['FAXQUEUEID']}'";
					$this->db->query($qSQL);


					if ($bUploaded) {
						$aFaxInQueue['SENDFAXQUEUEID'] = $cOptions['faxids'][0];
						$qSQL = "UPDATE `{$this->sFaxOutTable}` SET `FILEPATH` = '" . basename($aFaxInQueue['FILEPATH']) ."', `FILESIZE` = '{$aFaxInQueue['FILESIZE']}', `SENDFAXQUEUEID` = '{$aFaxInQueue['SENDFAXQUEUEID']}', `UPLOADEDEDDATE` = '" . $this->fnGetLocalTime() . "', BUPLOADED = '1' WHERE FAXQUEUEID = '{$aFaxInQueue['FAXQUEUEID']}'";
						$this->fnExecQuery($qSQL);

					}
					
				}				
			}


		} else {
			// no result for db
			if ($this->db->errno) {
				$this->fnSetFaxError($this->db->error);
			}
		}
	}

	

	/**
	* runs via cron: checks status of sent faxes from sfax
	* @param
	* @param void
	* @return array TSfax::aMessages();
	* @uses TSfax::fnSendFaxes()
	*/
	public function fnCronCheckFaxes(){
		$qSQL = "SELECT * FROM {$this->sFaxOutTable} WHERE BUPLOADED = 1 AND BSENT = 0";
		$result = $this->db->query($qSQL);
		$aPendingFaxes = array();
		while ($aRow = $result->fetch_assoc()) {
			$aPendingFaxes[] = $aRow; 
		}
		if (count($aPendingFaxes) > 0) {
			foreach ($aPendingFaxes as $key => $aPendingFax) {
				if (!empty($aPendingFax['SENDFAXQUEUEID'])) {
					$aGetStrings					= array();
					$aGetStrings['token']			= $this->GenerateSecurityTokenUrl();
					$aGetStrings['ApiKey']			= $this->sApiKey;
					$aGetStrings['SendFaxQueueId']  = $aPendingFax['SENDFAXQUEUEID'];
					
					$oResponse = $this->SendFaxStatus($aGetStrings);
					if (isset($oResponse->RecipientFaxStatusItems[0])) {
						$oFaxObject = $oResponse->RecipientFaxStatusItems[0];
						$aParams = array();
						$aParams[] = "`FAXID` = '"			.	$this->fnEscape($oFaxObject->FaxId) . "'";
						$aParams[] = "`ERRORCODE` = '"		.	$this->fnEscape($oFaxObject->ErrorCode) . "'";
						$aParams[] = "`RESULTMESSAGE` = '"	.	$this->fnEscape($oFaxObject->ResultMessage) . "'";
						$aParams[] = "`ATTEMPTS` = '"		.	$this->fnEscape($oFaxObject->Attempts) . "'";
						$aParams[] = "`FAXDATEISO` = '"		.	$this->fnEscape($oFaxObject->FaxDateIso) . "'";
						$aParams[] = "`WATERMARKID` = '"	.	$this->fnEscape($oFaxObject->WatermarkId) . "'";
						$aParams[] = "`RECIPIENTNAME` = '"	.	$this->fnEscape($oFaxObject->RecipientName) . "'";
						$aParams[] = "`FAXDATA` = '"		.	$this->fnEscape(serialize($oFaxObject)) . "'";
						
						if ($oFaxObject->IsSuccess == 1) {
							$aParams[] = "`BSENT` = '1'";
							$aParams[] = "`SENTTIME` = '" . $this->fnGetLocalTime() . "'";
						}
						
						$qSQL = "UPDATE {$this->sFaxOutTable} SET " . join(', ', $aParams) . " WHERE SENDFAXQUEUEID = '" . $this->fnEscape($aPendingFax['SENDFAXQUEUEID']) . "'";
						$this->db->query($qSQL);
						if ($this->db->errno) {
							$this->fnSetFaxError($this->db->error);
						}

					}

				}				
			}
		}

	}

	
	/**
	* returns true if number of faxes uploaded but not confirmed sent is at capacity
	* may also return the number of faxes awaiting sent confirmation
	*
	* @param bool $bFaxesWaiting - upon true will return the number of awaiting faxes
	* @return int|bool
	* @users TSfax::$iFaxQueueLimit
	*/
	public function fnIsAtCapacity($bFaxesWaiting = false){
		$iFaxesWaiting = 0;
		$qSQL = "SELECT COUNT(*) AS iFaxesWaiting FROM {$this->sFaxOutTable} WHERE BUPLOADED = 1 AND BSENT = 0";
		if ($result = $this->db->query($qSQL)) {			
			$oFaxesWaiting = $result->fetch_object();
			$iFaxesWaiting = $oFaxesWaiting->iFaxesWaiting;
			if ($bFaxesWaiting) {
				return (int)$iFaxesWaiting;
			} else {
				return ($this->iFaxQueueLimit < $iFaxesWaiting);
			}
		} else {
			$this->fnSetFaxError($this->db->error);
			return true;
		}

	}
	


	/**
	* class destructor
	* @param void
	* @return void
	*/
	public function __destruct (){
		$this->fnSetFaxSuccess('End Outgoing Fax Process');
		parent::__destruct();
	}
	
	


}
