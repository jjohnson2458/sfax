<?php
     
     error_reporting(E_ERROR | E_PARSE);
     
// configuration data removed for proprietary purposes
 
include_once(basePath."shared_scripts/loader_functions.php");
include(basePath."shared_scripts/globalstart.php");
fnStartPage($bEnd);

// Sfax takes over from here:

// calculate the correct root folder for the version ==============
$sFaxRootPath = dirname(dirname(__FILE__)) . DS . APP_VERSION . DS;

// since this cron only has one purpose, calcuate the exact include path ==================== 
$sSfaxPath = str_replace(DS . 'live' . DS, DS . APP_VERSION  . DS, thirdParty) . 'elgon' . DS . 'sfax' . DS;

// implement sfax owns autoloader ======================================

define('SFAX_PATH', $sSfaxPath);
if (!function_exists('fnSfaxAutoLoader')) {
	function fnSfaxAutoLoader($sClass){
		$sSfaxClassPath = SFAX_PATH . $sClass . '.php';
		if (!class_exists($sClass) && file_exists($sSfaxClassPath)) {			
			include_once $sSfaxClassPath;
		}
	}	
}
spl_autoload_register('fnSfaxAutoLoader');

// =====================================================================

// cron starts happens here =======================================================

$bIsCommandLine = (php_sapi_name() == 'cli');
$args = (!$bIsCommandLine && $_SERVER['REQUEST_METHOD'] == 'GET') ? $_GET : $argv;
$sSource = ($bIsCommandLine) ? $args[1] : $args['source'];
print_r($sSource);

$cOptions = array();// fill in the details later

define('SEND_FAX', true);
define('DOWNLOAD_FAX', true);
define('GET_FAX', true);
define('SEND_FAX', true);


//* for testing only 

if (!$bIsCommandLine) {
	echo '<pre>';
}

$bSfaxExists = (class_exists('TSfax')) ? true : false;
$TSfaxOutgoingCron = (class_exists('TSfaxOutgoingCron')) ? true : false;
$TSfaxIncomingCron = (class_exists('TSfaxIncomingCron')) ? true : false;
$aOutput['sFaxRootPath']		= $sFaxRootPath;
$aOutput['sSfaxPath']			= $sSfaxPath;
$aOutput['bTSfaxExists']		= $bSfaxExists;
$aOutput['TSfaxOutgoingCron']	= $TSfaxOutgoingCron;
$aOutput['TSfaxIncomingCron']	= $TSfaxIncomingCron;
$aOutput['args']				= $args; 
$aOutput['source']				= $sSource;
print_r($aOutput);







//error_reporting(E_ALL);

if (class_exists($sSource)) {
	$sfax = new $sSource();
	$sfax->clientId = 6;
	switch ($sSource) {
		case 'TSfaxIncomingCron': // for incoming faxes
			//$sfax->fnSetStartDate(date('m/d/Y', (time() - 345600)) );
			$sfax->fnSetStartDate(date('m/d/Y', strtotime('yesterday')));
			$sfax->fnGetFaxes();
		break;

		case 'TSfaxOutgoingCron': // for outgoing faxes
			$sfax->fnCronSendFaxes($cOptions);
			$sfax->fnCronCheckFaxes($cOptions);
		break;

		default :
		break;
	}

	$cmd = "chown -R apache:apache {$sfax->sInboundFaxPath} && chown -R gps:gps {$sfax->sOutboundFaxPath}";
	shell_exec($cmd);

}
//echo $sfax->debug($sfax);
	print_r($sfax->fnGetFaxMessages());

// cron ends here ==================================================================

die();

